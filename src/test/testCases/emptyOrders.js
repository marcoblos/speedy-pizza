module.exports = {
    'title': 'Empty orders test case',
    'description': 'This is a test with an empty data set to cover a basic scenario when we don\'t have any orders in json file',
    'result': [],
    'input': require('../data/emptyOrders.json')
}
