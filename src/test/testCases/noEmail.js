module.exports = {
    'title': 'No Email test case',
    'description': 'The data set used in this test cover cases which we don\'t have any email associated with valid and unique order',
    'result': [],
    'input': require('../data/noEmail.json')
}
