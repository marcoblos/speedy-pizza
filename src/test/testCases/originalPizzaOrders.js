module.exports = {
    'title': 'Assignment provided test case',
    'description': 'This data set was provided with the description of the assignment',
    'result': ['email2@example.com'],
    'input': require('../data/pizzaOrders.json')
}
