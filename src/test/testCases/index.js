module.exports = [
    require('./emptyOrders'),
    require('./moreThanOneValidOrderAtTheEnd'),
    require('./noEmail'),
    require('./onlyOneValidOrderWithEmail'),
    require('./onlyOneValidOrderWithoutEmail'),
    require('./originalPizzaOrders'),
    require('./repeatedToppingAcrossOrders'),
    require('./repeatedToppingSameOrder'),
    require('./unorderedTopping'),
]