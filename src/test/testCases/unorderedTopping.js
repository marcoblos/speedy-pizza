module.exports = {
    'title': 'Unordered toppings test case',
    'description': 'This test case cover scenarios where we don\'t have the toppings ordered',
    'result': ['email2@example.com'],
    'input': require('../data/unorderedToppings.json')
}
