module.exports = {
    'title': 'More than one valid order at the end of the data set test case',
    'description': 'All the valid unique orders should have been logged, even if they are the last ones in the data set',
    'result': ['email2@example.com', 'email3@example.com'],
    'input': require('../data/moreThanOneValidOrderAtTheEnd.json')
}
