module.exports = {
    'title': 'Only one valid order with email test case',
    'description': 'This data set cover cases when we have only one valid order with email',
    'result': ['email1@example.com'],
    'input': require('../data/onlyOneValidOrderWithEmail.json')
}
