module.exports = {
    'title': 'Repeated toppings across orders test case',
    'description': 'This data set cover cases when we have the exact same toppings combination in different orders',
    'result': ['garlic@example.com'],
    'input': require('../data/repeatedToppingAcrossOrders.json')
}
