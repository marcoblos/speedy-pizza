module.exports = {
    'title': 'Only one valid order without email test case',
    'description': 'This data set cover cases when we have only one valid order without email',
    'result': [],
    'input': require('../data/onlyOneValidOrderWithoutEmail.json')
}
