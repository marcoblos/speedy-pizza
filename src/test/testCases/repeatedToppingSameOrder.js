module.exports = {
    'title': 'Repeated topping in the same order test case',
    'description': 'This data set cover cases when we have a pizza with 3 toppings, but they are not unique',
    'result': ['email1@example.com'],
    'input': require('../data/repeatedToppingSameOrder.json')
}
