const algorithms = require('../algorithms')
const testCases = require('./testCases')

const getNewTestCases = () => {
    // I'm using JSON.parse and JSON.stringigy to make a deep copy of testCases
    // because I don't want any interference among different algorithms
    return JSON.parse(JSON.stringify(testCases))
}

describe('One in one million Challenge Test Suite', () => {

    beforeEach(() => {
        jest.spyOn(console, 'log')
    })

    afterEach(() => {
        console.log.mockClear()
    })

    algorithms.forEach((algorithm, i) => {
        getNewTestCases().forEach(testCase => {
            test(`algorithm: ${i + 1} -> test case: ${testCase.title}`, () => {
                // act
                algorithm(testCase.input)

                //assert
                expect(console.log).toHaveBeenCalledTimes(testCase.result.length)
                testCase.result.forEach((result, j) => {
                    expect(console.log).toHaveBeenNthCalledWith(j + 1, result)
                })
            })

        })
    })
})
