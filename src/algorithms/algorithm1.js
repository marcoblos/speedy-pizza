const sortToppings = require('./sortToppings')

function isValidWinner(email) {
    return !!email
}

function haveSameToppings(previousOrder, currentOrder) {
    return previousOrder.toppings.toString() === currentOrder.toppings.toString()
}

function notifyWinner(winner) {
    // Print out the email.
    console.log(winner)
}

function notifyUserIfShouldBeNotified(numberOfSimilarOrders, previousOrder) {
    if (numberOfSimilarOrders == 1 && isValidWinner(previousOrder.email)) {
        notifyWinner(previousOrder.email)
    }
}

function sortOrdersToppings(orders, sizeOfWinnerToppingsCombination) {
    return orders.map(order => {
        order.toppings = sortToppings(order.toppings)
        return order
    }).filter(order => order.toppings.length === sizeOfWinnerToppingsCombination ? order : undefined)
        // Perform a QuickSort on the array. We provide an anonymous function to do the comparisons.
        .sort((a, b) => a.toppings.toString().localeCompare(b.toppings.toString()))
}

function printWinners1(inputArray) {
    let previousOrder = {
        email: '',
        toppings: ''
    }
    let numberOfSimilarOrders = 0

    // Iterate through the array, with "order" being each item in the array.
    sortOrdersToppings(inputArray, 3)
        .map((order) => {
            if (haveSameToppings(previousOrder, order)) {
                numberOfSimilarOrders++
            } else {
                notifyUserIfShouldBeNotified(numberOfSimilarOrders, previousOrder)
                numberOfSimilarOrders = 1
            }
            previousOrder = { ...order }
        })

    notifyUserIfShouldBeNotified(numberOfSimilarOrders, previousOrder)
}

module.exports = printWinners1
