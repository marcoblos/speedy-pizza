function getUniqueToppingsCombination(toppings) {
    return [...new Set(toppings)]
}

module.exports = toppings => {
    return getUniqueToppingsCombination(toppings).sort((a, b) => {
        return a.localeCompare(b)
    })
}