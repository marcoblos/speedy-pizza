# Speedy Pizza

## Introduction

Speedy Pizza is a pizza chain with dozens of busy restaurants in a densely populated region. Their marketing department has come up with what they think is a brilliant promotion: the *One in a Million Challenge*.  Any customer that orders a three-topping pizza with a unique combination of toppings across all pizzas in all stores for that month will receive a coupon for a free pizza by email (if they provide their email address).
Each month they generate a file containing a JSON representation of all the pizzas ordered that month. The toppings for each pizza are sorted in alphabetical order [in this form](./pizzaOrders_original.json).

Speedy Pizza wants a routine to load this data and print out the email addresses of any winners.  (For the above example, it would print out email2@example.com.)  Two algorithms are suggested, represented by these implementations:

### [Implementation 1](./algorithm1_original.js)

### [Implementation 2](./algorithm2_original.js)

## Unit test and code analysis

To check if the algorithms are working properly, I implemented some unit tests using Jest, following this structure:

 - Each test case is performed once to each algorithm
 - Each test case is described inside src/test/testCases
 - Each test case is an object with three mandatory fields (title, result and input) and one optional field (description). The properties of each test case object are used to generate unit tests
 - Each test case has its own data set (input) and these data sets are located in src/test/data
 - The tests are generated inside this [test entrypoint](./src/test/algorithms.test.js)

To run the test cases I changed a little the algorithms.

 - both algorithms now are sorting and removing duplicates from toppings
 - the only change I did modify the code behavior to fix a bug
 - in the first algorithm, I decomposed the single function into small functions. I did it to improve the code readability.

### Plato - Static analysis and complexity tool

To analyze and generate a report from static source code I used [Plato](https://github.com/es-analysis/plato). After execute the _generate plato report_ command, as listed below, a folder named _src/report_ will be created, containing information about the analysis.

### Jest - code coverage

To generate a test coverage report is necessary to execute the _generate Jest coverage report_ command. When this command is completed, a folder named _src/coverage_ will be created, containing information about unit test coverage.

### Test and code analysis commands

```
## install dependencies
cd src
npm install

## execute tests
npm test

## execute tests and keep Jest whatching changes
npm run test:watch

## generate Jest coverage report
npm run test:coverage

## generate Plato report (for complexity analysis)
npm run report:generate
```

## Questions

1. Describe some data sets that you would use to test any proposed implementation and its accuracy.

    - [Empty orders test case](./src/test/testCases/emptyOrders.js)
    - [More than one valid order at the end of the data set test case](./src/test/testCases/moreThanOneValidOrderAtTheEnd.js)
    - [No Email test case](./src/test/testCases/noEmail.js)
    - [Only one valid order with email test case](./src/test/testCases/onlyOneValidOrderWithEmail.js)
    - [Only one valid order without email test case](./src/test/testCases/onlyOneValidOrderWithoutEmail.js)
    - [Assignment provided test case](./src/test/testCases/originalPizzaOrders.js)
    - [Repeated topping in same order test case](./src/test/testCases/repeatedToppingSameOrder.js)
    - [Repeated topping across orders test case](./src/test/testCases/repeatedToppingAcrossOrders.js)
    - [Unordered toppings test case](./src/test/testCases/unorderedTopping.js)

1. Did you identify any bugs in these proposed implementations?

    Ignoring bugs, these algorithms will only work correctly if we rely on these assumptions:

        1. The toppings are properly sorted
        2. There are no duplicated toppings in the same order

    Now, taking bugs into consideration, yes, in [this first proposed implementation](./algorithm1_original.js) occur an unexpected behavior when we have a data set which we have an *One in a Million* valid winner as the last entry in the data set, because the algorithm is always logging the previous order and not the current one, so in this case, the first algorithm won't work as expected.

1. Once any small bugs are fixed, will either of these approaches give us the output we're looking for?

    Yes. After adjust some points to enforce the desired behavior, both algorithms will work properly based on the test case provided with this assignment and with test cases described on the first question, above.

1. What are some advantages or disadvantages of these approaches?

    In the [first algorithm](./algorithm1_original.js), if we ignore the sorting part which occur between lines 3 and 6, this algorithm has the advantage to iterate just once around the data set. Unfortunately, the sorting part is important to achieve the correct behavior, otherwise it will not work consistently, because it needs to check the current element against the previous element and this process depends on the order of the elements. An advantage about this algorithm is that it is not necessary to use any extra memory.

    In the [second algorithm](./algorithm2_original.js), we have the advantage that we don't need to order the data set because the algorithm relies on an implementation of hashmap. A map has unique keys as a principle. If we want to check whether a combination of toppings has been processed or not, we just need to request this information to the map using the _get_ method. The downside of this second algorithm is the extra memory space its use to keep the keys and its values, if we think about a small data set to run unit tests, or even for a small business, it would not be a problem, but when we think about thousands and millions of orders, this approach could become a problem. One extra downside about this second algorithm is that we should iterate again over the map of topping combinations to check which one is duplicated or not.

1. If we were querying a database instead of working with an in-memory structure, would we use a significantly different algorithm to find the winners?  If so, how would you expect that algorithm to differ in performance and why?

    I think, it strongly depends on some factors: 
    
        - what database we are going to use (relational or non-relational database)
        - the data modeling
        - how the data is processed before it's persisted
        - how the data is persisted

    Considering a relational database we can think about some alternatives that are capable to return the winners only executing a query. It would change completely the current algorithm because the "heavy work" was executed by the database and then what the new algorithm need to do is only take care of the winner's notification. The performance will depends on what alternative will be adopted and how well some points will be covered, for instance: indexes and data modeling.

    Considering a non-relational database, I think the best we can do is apply correct filters and sort the dataset to minimize the algorithms workload. After sort and filter the dataset, the algorithm will receive a polished input, and it means that both algorithms will need less iterations to complete. If I'm not wrong, the non-relational databases and the relational databases should receive the same amount of attention in order to define indexes and to ensure a properly performance.

    Below, I just explained some alternatives and ideas about querying.

    1. Relational DB 1 - concatenate toppings in one column

        This approach consists in prepare the orders table, which the orders are persisted, to persist one more column called by _concatenatedToppings_. This column will receive a string containing the toppings separated by a comma.

        ```
            // array of toppings received
            // when the order is being processed
            [
                "Mushrooms",
                "Pepperoni",
                "Peppers"
            ]

            // toppings persisted in
            // concatenatedToppings column
            "Mushrooms,Pepperoni,Peppers"
        ```

        With this approach we could simple execute a query like this:

        ```
        SELECT id, email, toppings, COUNT(1)
        FROM orders
        WHERE date_insert BETWEEN '2020-01-10 00:00:00' AND '2020-02-10 23:59:59'
        GROUP BY toppings
        HAVING count(1) = 1;
        ```

        Then, we have in just one query the result we need, but also we would have some serious downsides here. If the order changes for some reason, we should update in more than one place the same information on the database, it will lead to possible data inconsistency. One more important point to be considered here is: to this implementation works properly we should know about this marketing promotion since the orders table was created, or we should create an insert in all legacy data to keep consistency, or at least on the period which the promotion is occurring.

    2. Relational DB 2 - execute a query

        This approach is more realistic, because in a scenario when we already have a normalized database structure, with different tables, we usually want to simple extract information from that persisted data. Considering a data model with three tables:

        - orders: keep basic information about orders
        - toppings: all possible toppings are described here
        - orders_toppings: each order has different toppings and these toppings are persisted in this table

        In this approach we simple execute the query below to retrieve the valid winners:

        ```
        SELECT id, email, toppings, COUNT(1)
        FROM (
            SELECT o.id, o.email, GROUP_CONCAT(t.description ORDER BY t.description) AS "toppings" 
            FROM orders o
            INNER JOIN orders_toppings ot ON ot.idOrder = o.id
            INNER JOIN toppings t ON t.id = ot.idToppings
            WHERE o.date_insert BETWEEN '2020-01-10 00:00:00' AND '2020-02-10 23:59:59'
            GROUP BY o.id
            ) temp
        GROUP BY toppings
        HAVING count(1) = 1;
        ```

        The downside here could be the performance, based on the size of the involved tables. We can also minimize the amount of work that the database would do adding some _WHERE_ clauses, like return only orders with three toppings.

    3. Relational DB 3 - create a view

        This approach is very similar than the approaches before. The only difference is that a view will be created and then the queries will be performed on the view instead of the tables.
        
        To create the view:

        ```
        CREATE VIEW view_orders_toppings_concatenated AS
        SELECT o.id, o.email, GROUP_CONCAT(t.description ORDER BY t.description) AS "toppings" 
            FROM orders o
            INNER JOIN orders_toppings ot ON ot.idOrder = o.id
            INNER JOIN toppings t ON t.id = ot.idToppings
            GROUP BY o.id;
        ```

        To retrieve the winners

        ```
        select * from view_orders_toppings_concatenated;
        SELECT id, email, toppings, COUNT(1)
        FROM view_orders_toppings_concatenated
        GROUP BY toppings
        HAVING count(1) = 1;
        ```

        Among the different relational approaches so far, this is my favorite when we think about a very simple dataset like the one we have here. Usually in the real world the data model is much more complicated, but for this scenario, the approach based on a view works great.

    4. Non-Relational DB 1

        I don't have experience querying non-relational databases like Dynamo DB. I thought about search and learn some simple commands, but if I do it, I'll postpone too much the delivery of this test, and also the timing is important.

        If I'm not wrong, after a really quick search, I found that DynamoDB doesn't support aggregation functions like _group by_, so my approach here is use the DynamoDB to filter and sort the data I want.
        
        As a filter I could use
        
         - toppings quantity equals 'N'
         - insert date between 'N' and 'M'
         - email exists

        In the sort I would like to

         - sort toppings inside each order 
         - sort orders by order toppings

        If this approach really works in this way, I can use the retrieved data set as the algorithm's input, and then I would be able to remove the sorting functions, relying on the input.


## Final thoughts

I put the maximum effort and time I could put in these last two busy days, and also did my best to remember more about Javascript, NodeJs and its ecosystem (I'm approximately one and a half year without use Javascript).

About this document in general, we always have a lot of different ways to solve a problem. All proposed alternatives and analyses expressed here in this document are in constant change. This document is a mix of experience and research, which means that it is always can be improved by more experience and more study.

Thanks for the opportunity and if you have any questions, please let me know.

author: marcoblos1234@gmail.com